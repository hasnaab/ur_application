# ur_application

## Initial setup
1. Clone `https://github.com/ros-industrial/universal_robot.git` in `catkin_ws/src`
2. Clone this repo (or your fork) in `catkin_ws/src`
3. From `catkin_ws`, run `rosdep install --from-paths src --ignore-src -r -y` to install system dependencies
4. Add a `conanfile.txt` at the workspace root with the following content:
```
[requires]
robot-model/1.0@bnavarro/testing
robot-trajectory/1.0@bnavarro/testing
robot-control/1.0@bnavarro/testing

[generators]
cmake
```
5. Add the `bnavarro` Conan remote: `conan remote add bnavarro https://api.bintray.com/conan/benjaminnavarro/bnavarro`
6. Get the Conan dependencies: `cd catkin_ws/build && conan install .. --build=missing`

## Build & install
Just run `catkin_make install` from `catkin_ws`

## Run
1. Make sure to source `catkin_ws/install/setup.bash` or `catkin_ws/install/setup.zsh` depending on your shell.
2. Launch the `dual_arm.launch` file: `roslaunch ur_application dual_arm.launch`

This launch file will load two UR10 arms inside the Gazebo simulator with joint position controllers.